import React from "react";
import styled from "styled-components";

const StyledButton = styled.a`
  display: inline-block;
  width: 155px;
  height: 50px;
  border-radius: 25px;
  margin-top: 15px;
  line-height: 50px;
  text-align: center;
  font-size: 16px;
  color: #fff;
  box-shadow: 0px 14px 30px rgba(69, 158, 255, 0.2);
  text-decoration: none;
`;
export default (props) => {
  return (
    <StyledButton
      href="#"
      onClick={(e) => {
        if (props.onClick) {
          e.preventDefault();
          props.onClick(e);
          F;
        }
      }}
      style={{
        backgroundColor: props.color,
        color: props.textColor ? props.textColor : "#fff",
      }}
      target="_blank"
    >
      {props.text}
    </StyledButton>
  );
};
