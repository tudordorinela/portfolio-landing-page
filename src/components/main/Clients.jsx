import React from "react";
import styled from "styled-components";
import Variables from "../../variables.js";
import NewsLogo from "../../assets/news-logo.svg";
import BusinessInsider from "../../assets/bi-logo.svg";
import Envato from "../../assets/envato-logo.svg";
import Midweek from "../../assets/midweek-logo.svg";
import Wired from "../../assets/wired-logo.svg";

const StyledTestimonials = styled.div`
  z-index: 2;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 50px 10px 0px 10px;

  .text-wrapper {
    text-align: center;

    .subtitle {
      margin: 0;
      margin-bottom: 10px;
      font-size: 18px;
      line-height: 27px;
      color: ${Variables.colors.textPrimary};
    }
    .title {
      margin: 0;
      margin-bottom: 10px;
      font-size: 60px;
      font-weight: bold;
      line-height: 80px;
      color: ${Variables.colors.textPrimary};
      letter-spacing: -1.85143px;

      ${Variables.media.touch} {
        font-size: 32px;
      }
    }
    .descr {
      margin: 0px;
      font-size: 15px;
      line-height: 24px;
      color: ${Variables.colors.textSecondary};
      max-width: 332px;
      margin: 0 auto;
    }
  }

  .clients-logos {
    margin: 70px 0 50px 0;
    text-align: center;
  }
`;

export default () => {
  return (
    <StyledTestimonials>
      <div className="text-wrapper">
        <p className="subtitle">Clients</p>
        <p className="title">Some notable clients</p>
        <p className="descr">
          Freelance UI/UX Designer, also passionate in making beautiful
          illustrations and icons
        </p>
      </div>

      <div className="clients-logos">
        <a href="#" target="_blank">
          <img src={NewsLogo} alt="" />
        </a>
        <a href="#" target="_blank">
          <img src={BusinessInsider} alt="" />
        </a>
        <a href="#" target="_blank">
          <img src={Envato} alt="" />
        </a>
        <a href="#" target="_blank">
          <img src={Midweek} alt="" />
        </a>
        <a href="#" target="_blank">
          <img src={Wired} alt="" />
        </a>
      </div>
    </StyledTestimonials>
  );
};
