import React, { useState } from "react";
import styled from "styled-components";
import Button from "../common/Button.jsx";
import Variables from "../../variables.js";
import DotsWhite from "../../assets/dots-white.svg";
import DotsBlue from "../../assets/dots-blue.svg";
import Doodle1 from "../../assets/doodle-1-blue.svg";
import Doodle2 from "../../assets/doodle-2-blue.svg";
import Doodle3 from "../../assets/doodle-3-blue.svg";
import Doodle4 from "../../assets/doodle-4-blue.svg";
import Profile from "../../assets/profile.png";
import Skill from "../common/Skill.jsx";
import UiDesign from "../../assets/ui-design.svg";
import Seo from "../../assets/seo.svg";
import SocialMedia from "../../assets/social-media.svg";

const StyledAbout = styled.div`
  overflow: hidden;
  position: relative;

  .about {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 160px 150px 215px 150px;
    position: relative;

    ${Variables.media.desktop} {
      justify-content: center;
      padding: 160px 0px 207px 0px;
    }
    ${Variables.media.touch} {
      flex-direction: column;
      padding: 160px 0px 114px 0px;
    }
    ${Variables.media.mobile} {
      padding: 100px 20px 50px 20px;
    }

    .text-wrapper {
      width: 386px;
      margin-left: 45px;

      ${Variables.media.mobile} {
        width: 100%;
        text-align: center;
        margin: 0;
        margin-left: 0px;
      }
      ${Variables.media.tablet} {
        margin-left: 0px;
        margin-top: 100px;
      }

      .subtitle {
        margin: 0;
        margin-bottom: 10px;
        font-size: 18px;
        line-height: 27px;
        color: ${Variables.colors.textPrimary};
      }

      .title {
        margin: 0;
        margin-bottom: 30px;
        font-size: 60px;
        font-weight: bold;
        line-height: 80px;
        color: ${Variables.colors.textPrimary};

        ${Variables.media.touch} {
          font-size: 32px;
          margin-bottom: 10px;
        }
      }

      .categories {
        .category {
          .category-title {
            display: inline-block;
            margin-right: 40px;
            margin-bottom: 30px;
            font-size: 18px;
            line-height: 23px;
            color: ${Variables.colors.textTernary};
            cursor: pointer;
            user-select: none;
            border-bottom: 2px solid transparent;
            transition: border-bottom 0.5s linear;
            &.active {
              border-bottom: 2px solid ${Variables.colors.primary};
            }

            ${Variables.media.mobile} {
              font-size: 16px;
              margin-right: 20px;
            }
          }
        }

        .descr-wrapper {
          position: relative;
          height: 150px;
          ${Variables.media.mobile} {
            height: 190px;
          }

          .descr {
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
            transition: opacity 0.5s linear;
            margin-top: 0px;
            font-size: 15px;
            line-height: 24px;
            color: ${Variables.colors.textSecondary};
            &.active {
              opacity: 1;
            }
          }
        }
      }
    }

    ${Variables.animation}
    .square-image-wrapper {
      position: relative;
      .doodle1 {
        position: absolute;
        top: 20px;
        left: -69px;
        animation: float 90s linear infinite;
      }

      .doodle2 {
        position: absolute;
        top: -64px;
        left: 55px;
        animation: float 40s linear infinite;
        animation-delay: -10s;
      }
      .doodle3 {
        position: absolute;
        top: 122px;
        left: -46px;
        animation: float 70s linear infinite;
        animation-delay: -35s;
      }
      .doodle4 {
        position: absolute;
        bottom: -40px;
        right: 95px;
        animation: float 50s linear infinite;
        animation-delay: -40s;
      }
      ${Variables.media.mobile} {
        width: 100%;
      }

      .square-image {
        width: 420px;
        border-radius: 18px;
        margin-right: 45px;
        background: ${Variables.colors.bgshapes};
        box-shadow: -40px 2px 80px rgba(0, 0, 0, 0.5);
        overflow: hidden;
        display: flex;

        ${Variables.media.tablet} {
          margin-right: 0px;
          margin-bottom: 30px;
        }

        ${Variables.media.mobile} {
          width: 100%;
          margin-left: 0px;
          margin-bottom: 50px;
        }
        img {
          width: 100%;
          height: auto;
        }
      }
    }
  }

  .expertise {
    z-index: 2;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    padding: 0px 20px 85px 20px;

    .text-wrapper {
      text-align: center;

      .subtitle {
        margin: 0;
        margin-bottom: 10px;
        font-size: 18px;
        line-height: 27px;
        color: ${Variables.colors.textPrimary};
      }

      .title {
        margin: 0;
        margin-bottom: 10px;
        font-size: 60px;
        font-weight: bold;
        line-height: 80px;
        color: ${Variables.colors.textPrimary};
        letter-spacing: -1.85143px;

        ${Variables.media.touch} {
          font-size: 32px;
        }
      }

      .descr {
        margin: 0px;
        font-size: 15px;
        line-height: 24px;
        color: ${Variables.colors.textSecondary};
        max-width: 332px;
      }
    }

    .skills {
      display: flex;
      justify-content: center;
      align-items: center;
      flex-wrap: wrap;
    }
  }

  .bg {
    z-index: -1;
    position: absolute;
    top: 0;
    left: 0;
    overflow: hidden;
    width: 100vw;
    height: 1437px;
    ${Variables.media.mobile} {
      display: none;
    }

    .rectangle {
      z-index: 1;
      position: absolute;
      left: -362px;
      top: 363px;
      width: 724px;
      height: 724px;
      background: ${Variables.colors.bgImageColor};
      border-radius: 100px;
      transform: rotate(-45deg);
    }

    .dotsWhite {
      z-index: 2;
      position: absolute;
      left: 60px;
      top: 491px;
      height: 178px;
    }

    .dotsBlue {
      z-index: 2;
      position: absolute;
      right: 86px;
      bottom: 0;
      height: 178px;
      ${Variables.media.touch} {
        display: none;
      }
    }
  }
`;

export default () => {
  const data = [
    {
      title: "Life",
      descr:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt rem, a molestias non optio consequuntur sequi similique et nesciunt sint dolor ipsa repudiandae nemo corporis id deleniti vitae autem beatae?",
    },
    {
      title: "Education",
      descr:
        "Pacific housing unique experiences things to do motel nature flexibility international bus place to stay miles kayak overnight territory. Chartering dollar nighttrain city trip. Cuba caravan Brasil Australia guide place to stay horse riding camper Budapest.",
    },
    {
      title: "Experience",
      descr:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt rem, a molestias non optio consequuntur sequi similique et nesciunt sint dolor ipsa repudiandae nemo corporis id deleniti vitae autem beatae?",
    },
  ];

  const [current, setCurrent] = useState(0);
  const [old, setOld] = useState(1);
  const handleClick = (i) => () => {
    setOld(current);
    setTimeout(() => {
      setCurrent(i);
    }, 500);
  };

  return (
    <StyledAbout>
      <div className="about">
        <div className="square-image-wrapper">
          <img className="doodle1" src={Doodle1} alt="" />
          <img className="doodle2" src={Doodle2} alt="" />
          <img className="doodle3" src={Doodle3} alt="" />
          <img className="doodle4" src={Doodle4} alt="" />
          <div className="square-image">
            <img src={Profile} alt="" />
          </div>
        </div>

        <div className="text-wrapper">
          <p className="subtitle">A bit</p>
          <p className="title">About me</p>
          <div className="categories">
            <div className="category">
              {data.map((d, i) => (
                <div
                  key={i}
                  className={
                    "category-title " +
                    (i == current && i != old ? "active " : "")
                  }
                  onClick={handleClick(i)}
                >
                  {d.title}
                </div>
              ))}
            </div>
            <div className="descr-wrapper">
              {data.map((d, i) => (
                <p
                  key={i}
                  className={
                    "descr " + (i == current && i != old ? "active " : "")
                  }
                >
                  {d.descr}
                </p>
              ))}
            </div>
          </div>
          <Button color={Variables.colors.primary} text="Download CV" />
        </div>
      </div>
      <div className="expertise">
        <div className="text-wrapper">
          <p className="subtitle">Skills</p>
          <p className="title">Expertise</p>
          <p className="descr">
            Freelance UI/UX Designer, also passionate in making beautiful
            illustrations and icons
          </p>
        </div>
        <div className="skills">
          <Skill
            image={UiDesign}
            title="UI/UX Design"
            descr="Launch party pitch technology user experience innovator buzz stealth MVP business model."
          />
          <Skill
            image={Seo}
            title="Local SEO"
            descr="Launch party pitch technology user experience innovator buzz stealth MVP business model."
          />
          <Skill
            image={SocialMedia}
            title="Social Media Marketing"
            descr="Launch party pitch technology user experience innovator buzz stealth MVP business model."
          />
        </div>
      </div>
      <div className="bg">
        <div className="rectangle"></div>
        <img className="dotsWhite" src={DotsWhite} alt="" />
        <img className="dotsBlue" src={DotsBlue} alt="" />
      </div>
    </StyledAbout>
  );
};
