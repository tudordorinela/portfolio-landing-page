import React from "react";
import styled from "styled-components";
import Button from "../common/Button.jsx";
import DotsGray from "../../assets/dots-gray.svg";
import DotsWhite from "../../assets/dots-white.svg";
import Variables from "../../variables.js";
import Doodle1 from "../../assets/doodle-1.svg";
import Doodle2 from "../../assets/doodle-2.svg";
import Doodle3 from "../../assets/doodle-3.svg";
import Doodle4 from "../../assets/doodle-4.svg";
import Profile from "../../assets/profile.png";

const StyledHero = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 236px 205px 115px 250px;
  overflow: hidden;

  ${Variables.media.desktop} {
    justify-content: center;
    padding: 236px 0px 115px 0px;
  }
  ${Variables.media.touch} {
    flex-direction: column;
    padding: 236px 20px 0px 20px;
  }
  ${Variables.media.mobile} {
    padding: 100px 20px 100px 20px;
  }

  .text-wrapper {
    width: 332px;
    margin-right: 36px;
    ${Variables.media.mobile} {
      width: 100%;
      text-align: center;
      margin: 0;
    }

    ${Variables.media.touch} {
      :nth-of-type(1) {
        order: 2;
        margin-right: 0px;
      }
    }

    .subtitle {
      margin: 0;
      margin-bottom: 10px;
      font-size: 18px;
      line-height: 27px;
      color: ${Variables.colors.textPrimary};
    }

    .title {
      margin: 0;
      margin-bottom: 10px;
      font-size: 60px;
      font-weight: bold;
      line-height: 80px;
      color: ${Variables.colors.textPrimary};

      ${Variables.media.touch} {
        font-size: 32px;
      }
    }

    .descr {
      margin-top: 0px;
      font-size: 15px;
      line-height: 24px;
      color: ${Variables.colors.textSecondary};
    }
  }

  ${Variables.animation}
  .round-image-wrapper {
    position: relative;
    .doodle1 {
      position: absolute;
      top: -58px;
      right: 210px;
      animation: float 90s linear infinite;
    }
    .doodle2 {
      position: absolute;
      top: 180px;
      right: -110px;
      animation: float 40s linear infinite;
      animation-delay: -10s;
    }
    .doodle3 {
      position: absolute;
      top: -41px;
      right: -113px;
      animation: float 70s linear infinite;
      animation-delay: -35s;
    }
    .doodle4 {
      position: absolute;
      top: 20px;
      right: 4px;
      animation: float 50s linear infinite;
      animation-delay: -40s;
    }
    ${Variables.media.mobile} {
      width: 100%;
    }

    .round-image {
      width: 420px;
      border-radius: 50%;
      margin-left: 36px;
      background: ${Variables.colors.bgshapes};
      box-shadow: 40px 2px 80px rgba(0, 0, 0, 0.5);
      overflow: hidden;
      display: flex;

      ${Variables.media.tablet} {
        margin-left: 0px;
        margin-bottom: 30px;
      }

      ${Variables.media.mobile} {
        width: 100%;
        margin-left: 0px;
        margin-bottom: 50px;
      }
      img {
        width: 100%;
        height: auto;
      }
    }
  }

  .bg {
    z-index: -1;
    position: absolute;
    top: 0;
    right: 0;
    overflow: hidden;
    width: 100vw;
    height: 750px;
    ${Variables.media.mobile} {
      display: none;
    }

    .rectangle {
      z-index: 1;
      position: absolute;
      right: -606px;
      top: -335px;
      width: 990px;
      height: 990px;
      background: ${Variables.colors.bgImageColor};
      border-radius: 100px;
      transform: rotate(-45deg);
    }
    .dotsGray {
      z-index: 2;
      position: absolute;
      right: 336px;
      top: -30px;
      height: 238px;
      mix-blend-mode: normal;
      opacity: 0.27;
    }
    .dotsWhite {
      z-index: 2;
      position: absolute;
      right: 69px;
      top: 478px;
      height: 238px;
    }
  }
`;

export default () => {
  return (
    <StyledHero>
      <div className="text-wrapper">
        <p className="subtitle">Hello</p>
        <p className="title">I’m Orion</p>
        <p className="descr">
          Freelance UI/UX Designer, also passionate in making beautiful
          illustrations and icons
        </p>
        <Button color={Variables.colors.primary} text="Hire me" />
      </div>
      <div className="round-image-wrapper">
        <img className="doodle1" src={Doodle1} alt="" />
        <img className="doodle2" src={Doodle2} alt="" />
        <img className="doodle3" src={Doodle3} alt="" />
        <img className="doodle4" src={Doodle4} alt="" />
        <div className="round-image">
          <img src={Profile} alt="" />
        </div>
      </div>
      <div className="bg">
        <div className="rectangle"></div>
        <img className="dotsGray" src={DotsGray} alt="" />
        <img className="dotsWhite" src={DotsWhite} alt="" />
      </div>
    </StyledHero>
  );
};
