import React, { useState } from "react";
import styled from "styled-components";
import Logo from "../../assets/logo.svg";
import Facebook from "../../assets/facebook.svg";
import Instagram from "../../assets/instagram.svg";
import Twitter from "../../assets/twitter.svg";
import Variables from "../../variables.js";

const StyledHeader = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 75px;

  .menuOpen {
    display: none;
    width: 55px;
    height: 55px;
    cursor: pointer;
    position: absolute;
    left: 100px;
    top: 10px;
    span {
      width: 55px;
      height: 3px;
      position: absolute;
      top: 26px;
      left: 0px;
      background: #000;
      &:before {
        content: "";
        width: 55px;
        height: 3px;
        background: #000;
        position: absolute;
        top: -20px;
        right: 0;
      }

      &:after {
        content: "";
        width: 55px;
        height: 3px;
        background: #000;
        position: absolute;
        top: 20px;
        right: 0;
      }
    }

    ${Variables.media.touch} {
      display: block;
    }
    ${Variables.media.mobile} {
      left: 20px;
    }
    &.open {
      background: blue;
    }
  }

  .logo {
    padding-right: 60px;
    padding-left: 160px;
    display: inline-block;
    ${Variables.media.touch} {
      padding-left: 20px;
      padding-right: 0px;
    }
    ${Variables.media.mobile} {
      position: absolute;
      right: 20px;
    }
    img {
      margin-top: 10px;
    }
  }

  nav {
    height: 75px;
    display: inline-flex;
    position: absolute;
    ${Variables.media.touch} {
      width: 100vw;
      height: 100vh;
      z-index: 99;
      position: fixed;
      top: 0;
      left: 0;
      pointer-events: none;
      &.open {
        pointer-events: all;
        ul {
          left: 0vw;
        }
      }
    }

    ul {
      display: inline-block;
      transition: left 1s linear;
      padding: 0;
      margin: 0;

      ${Variables.media.touch} {
        position: absolute;
        top: 0;
        left: -150vw;
        background: ${Variables.colors.primary};
        width: 100vw;
        height: 100vh;
      }
      .menuClosed {
        display: none;
        ${Variables.media.touch} {
          display: block;
        }
        width: 55px;
        height: 55px;
        position: absolute;
        top: 10px;
        left: 10px;
        cursor: pointer;
        span {
          position: absolute;
          left: 0;
          top: 26px;
          width: 55px;
          &:before {
            content: "";
            width: 55px;
            height: 3px;
            background: #fff;
            position: absolute;
            top: 0px;
            right: 0;
            transform: rotate(45deg);
          }

          &:after {
            content: "";
            width: 55px;
            height: 3px;
            background: #fff;
            position: absolute;
            top: 0px;
            right: 0;
            transform: rotate(-45deg);
          }
        }
      }
      li {
        display: inline-block;
        list-style-type: none;
        ${Variables.media.touch} {
          width: 100%;
          text-align: center;
        }
        a {
          margin-right: 40px;
          color: ${Variables.colors.textPrimary};
          font-size: 16px;
          font-weight: 500;
          line-height: 75px;
          text-decoration: none;
          ${Variables.media.touch} {
            color: #fff;
          }
        }
      }
    }
  }

  .social-buttons {
    position: absolute;
    top: 17px;
    right: 150px;
    z-index: 100;
    ${Variables.media.touch} {
      right: 20px;
    }
    ${Variables.media.mobile} {
      display: none;
    }
    a {
      padding: 0 7px;
    }
  }
`;

export default () => {
  const [menuOpen, setMenuOpen] = useState(false);
  return (
    <StyledHeader>
      <div className="menuOpen" onClick={() => setMenuOpen(true)}>
        <span></span>
      </div>
      <a href="#" className="logo">
        <img src={Logo} alt="" />
      </a>
      <nav className={menuOpen ? "open" : ""}>
        <ul>
          <li>
            <a
              href="#"
              onClick={(e) => {
                setMenuOpen(false);
                e.preventDefault();
                document
                  .querySelector(".about")
                  .scrollIntoView({ behavior: "smooth" });
              }}
            >
              About
            </a>
          </li>
          <li>
            <a
              href="#"
              onClick={(e) => {
                setMenuOpen(false);
                e.preventDefault();
                document
                  .querySelector(".portfolioSection")
                  .scrollIntoView({ behavior: "smooth" });
              }}
            >
              Portfolio
            </a>
          </li>
          <li>
            <a
              href="#"
              onClick={(e) => {
                setMenuOpen(false);
                e.preventDefault();
                document
                  .querySelector(".form")
                  .scrollIntoView({ behavior: "smooth" });
              }}
            >
              Contact
            </a>
          </li>
          <div className="menuClosed" onClick={() => setMenuOpen(false)}>
            <span></span>
          </div>
        </ul>
      </nav>
      <div className="social-buttons">
        <a href="https://facebook.com" target="_blank">
          <img src={Facebook} />
        </a>
        <a href="https://instagram.com" target="_blank">
          <img src={Instagram} />
        </a>
        <a href="https://twitter.com" target="_blank">
          <img src={Twitter} />
        </a>
      </div>
    </StyledHeader>
  );
};
