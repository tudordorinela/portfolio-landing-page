import React, { useState } from "react";
import styled from "styled-components";
import Button from "../common/Button";
import Variables from "../../variables.js";
import Icon from "../../assets/testimony-icon.svg";
import DotsWhite from "../../assets/dots-white-big.svg";
import DotsBlue from "../../assets/dots-blue-big.svg";
import Doodle1 from "../../assets/doodle-1-blue.svg";
import Doodle2 from "../../assets/doodle-2.svg";
import Doodle3 from "../../assets/doodle-3-blue.svg";
import Doodle4 from "../../assets/doodle-4.svg";

const StyledContact = styled.div`
  overflow: hidden;
  position: relative;
  padding-top: 40px;
  position: relative;
  top: -40px;

  .testimonials {
    display: flex;
    justify-content: space-between;
    align-items: top;
    padding: 0px 192px 0px 193px;
    position: relative;

    ${Variables.media.touch} {
      flex-direction: column;
      justify-content: center;
      align-items: center;
      padding: 0px 20px 0px 20px;
    }
    ${Variables.media.desktop} {
      padding: 0px 100px 0px 100px;
    }

    .square-image-wrapper {
      .square-image {
        width: 306px;
        height: 369px;
        border-radius: 18px;
        margin-right: 57px;
        background: ${Variables.colors.bgshapes};
        box-shadow: 40px 40px 80px rgba(0, 0, 0, 0.5);

        ${Variables.media.touch} {
          margin-bottom: 50px;
          margin-right: 0px;
        }

        img {
          width: 100%;
          height: auto;
        }
      }
    }

    .categories {
      width: 475px;
      margin-left: 57px;
      ${Variables.media.mobile} {
        width: 100%;
        margin-left: 0px;
      }

      .descr-wrapper {
        position: relative;
        height: 250px;
        padding-bottom: 45px;
        ${Variables.media.mobile} {
          width: 100%;
          height: 400px;
        }

        .descr {
          position: absolute;
          top: 0;
          left: 0;
          opacity: 0;
          transition: opacity 1s linear;
          margin-top: 0px;
          font-style: normal;
          font-weight: normal;
          font-size: 18px;
          line-height: 27px;
          color: ${Variables.colors.textPrimary};
          &.active {
            opacity: 1;
          }
          .quote {
            margin-bottom: 30px;
          }

          .name {
            color: #000000;
            font-style: normal;
            font-size: 21px;
            line-height: 26px;
            font-weight: 200;
          }
        }
      }

      .selection {
        .line {
          display: inline-block;
          width: 50px;
          height: 6px;
          margin-right: 8px;
          margin-bottom: 30px;
          background: #dbdbdb;
          cursor: pointer;
          transition: all 1s linear;
          &.active {
            background: #3b43f2;
          }
        }
      }
    }
  }
  .form {
    display: flex;
    justify-content: center;
    margin-top: 172px;
    margin-bottom: 110px;

    .container {
      box-sizing: border-box;
      max-width: 644px;
      width: 100%;
      padding: 60px 95px;
      border-radius: 18px;
      background: #fff;
      box-shadow: 0px 20px 60px rgba(0, 0, 0, 0.2);
      position: relative;

      ${Variables.media.mobile} {
        margin: 0 20px;
        padding: 20px 20px;
      }
      .doodle1 {
        position: absolute;
        top: 256px;
        left: -196px;
        animation: float 90s linear infinite;
      }
      .doodle2 {
        position: absolute;
        top: 204px;
        right: -153px;
        animation: float 40s linear infinite;
        animation-delay: -10s;
      }
      .doodle3 {
        position: absolute;
        top: 361px;
        left: -152px;
        animation: float 70s linear infinite;
        animation-delay: -35s;
      }
      .doodle4 {
        position: absolute;
        top: 305px;
        right: -57px;
        animation: float 50s linear infinite;
        animation-delay: -40s;
      }
      h1 {
        text-align: center;
        font-size: 18px;
        line-height: 27px;
        font-style: normal;
        font-weight: bold;
        color: ${Variables.colors.textPrimary};
        margin-bottom: 45px;
      }
      .field {
        input,
        textarea {
          font-family: Livvic;
          font-size: 15px;
          width: 100%;
          padding: 10px 6px;
          margin-bottom: 39px;
          border: 0;
          border-bottom: 1px solid #aeb7c1;
        }
        textarea {
          height: 64px;
        }
      }
      .button {
        text-align: center;
      }
    }
  }
  .bg {
    z-index: -1;
    position: absolute;
    top: 40px;
    left: 0;
    overflow: hidden;
    width: 100vw;
    height: 1437px;
    ${Variables.media.touch} {
      display: none;
    }

    .rectangle {
      z-index: 1;
      position: absolute;
      right: -363px;
      top: 403px;
      width: 724px;
      height: 724px;
      background: ${Variables.colors.bgImageColor};
      border-radius: 100px;
      transform: rotate(-45deg);
    }
    .circle {
      z-index: 1;
      position: absolute;
      left: -146px;
      top: 185px;
      width: 527px;
      height: 527px;
      background: ${Variables.colors.bgImageColor};
      border-radius: 50%;
    }

    .dotsWhite {
      z-index: 2;
      position: absolute;
      left: 74px;
      top: 248px;
      height: 238px;
    }

    .dotsBlue {
      z-index: 2;
      position: absolute;
      right: 249px;
      top: 449px;
      height: 198px;
    }
  }
`;

export default () => {
  const data = [
    {
      image: "",
      name: "Silvia Natalia1",
      position: "Owner Tanahcon",
      qoute:
        "“1Lean startup metrics venture innovator assets angel investor learning curve incubator branding advisor termsheet. IPad ecosystem conversion android advisor. Incubator vesting period metrics crowdfunding backing interaction design business model canvas strategy”",
    },
    {
      image: "",
      name: "Silvia Natalia2",
      position: "Owner Tanahcon",
      qoute:
        "“2Lean startup metrics venture innovator assets angel investor learning curve incubator branding advisor termsheet. IPad ecosystem conversion android advisor. Incubator vesting period metrics crowdfunding backing interaction design business model canvas strategy”",
    },
    {
      image: "",
      name: "Silvia Natalia3",
      position: "Owner Tanahcon",
      qoute:
        "“3Lean startup metrics venture innovator assets angel investor learning curve incubator branding advisor termsheet. IPad ecosystem conversion android advisor. Incubator vesting period metrics crowdfunding backing interaction design business model canvas strategy”",
    },
    {
      image: "",
      name: "Silvia Natalia4",
      position: "Owner Tanahcon",
      qoute:
        "“4Lean startup metrics venture innovator assets angel investor learning curve incubator branding advisor termsheet. IPad ecosystem conversion android advisor. Incubator vesting period metrics crowdfunding backing interaction design business model canvas strategy”",
    },
  ];

  const [active, setActive] = useState(0);
  const handleClick = (i) => () => setActive(i);
  return (
    <StyledContact>
      <div className="testimonials">
        <div className="square-image-wrapper">
          <div className="square-image"></div>
        </div>

        <div className="categories">
          <img src={Icon} alt="" />
          <div className="descr-wrapper">
            {data.map((d, i) => (
              <div key={i} className={"descr " + (i == active ? "active" : "")}>
                <p className="quote">{d.qoute}</p>
                <p className="name">
                  <strong> {d.name}</strong> <br />
                  {d.position}
                </p>
              </div>
            ))}
          </div>
          <div className="selection">
            {data.map((d, i) => (
              <div
                key={i}
                className={"line " + (i == active ? "active" : "")}
                onClick={handleClick(i)}
              ></div>
            ))}
          </div>
        </div>
      </div>
      <div className="form">
        <form className="container">
          <img className="doodle1" src={Doodle1} alt="" />
          <img className="doodle2" src={Doodle2} alt="" />
          <img className="doodle3" src={Doodle3} alt="" />
          <img className="doodle4" src={Doodle4} alt="" />
          <h1>Fell free to contact me and say hello!</h1>
          <div className="field">
            <input type="text" name="text" placeholder="Your name" required />
          </div>
          <div className="field">
            <input
              type="text"
              name="email"
              placeholder="E-mail address"
              required
            />
          </div>
          <div className="field">
            <textarea
              name="message"
              placeholder="Your message here…"
              required
            />
          </div>
          <div className="button">
            <Button color={Variables.colors.primary} text="Send Message" />
          </div>
        </form>
      </div>
      <div className="bg">
        <div className="rectangle"></div>
        <div className="circle"></div>
        <img className="dotsWhite" src={DotsWhite} alt="" />
        <img className="dotsBlue" src={DotsBlue} alt="" />
      </div>
    </StyledContact>
  );
};
