import React from "react";
import styled from "styled-components";
import Facebook from "../../assets/facebook-blue.svg";
import Instagram from "../../assets/instagram-blue.svg";
import Twitter from "../../assets/twitter-blue.svg";
import Variables from "../../variables.js";

const StyledFooter = styled.div`
  ul {
    max-width: 386px;
    margin: 0 auto;
    text-align: center;
    li {
      display: inline-block;
      list-style-type: none;
      a {
        margin-right: 40px;
        color: ${Variables.colors.textPrimary};
        font-size: 16px;
        font-weight: 500;
        line-height: 75px;
        text-decoration: none;
      }
    }
  }
  .social {
    max-width: 174px;
    margin: 0 auto;
    a {
      margin: 0 7px;
    }
  }
  .design {
    width: 100%;
    text-align: center;
    margin: 30px 0 60px 0;
  }
`;

export default () => {
  return (
    <StyledFooter>
      <ul>
        <li>
          <a
            onClick={(e) => {
              e.preventDefault();
              document
                .querySelector("#root")
                .scrollIntoView({ behavior: "smooth" });
            }}
            href="#"
          >
            Home
          </a>
        </li>
        <li>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              document
                .querySelector(".about")
                .scrollIntoView({ behavior: "smooth" });
            }}
          >
            About
          </a>
        </li>
        <li>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              document
                .querySelector(".portfolioSection")
                .scrollIntoView({ behavior: "smooth" });
            }}
          >
            Portfolio
          </a>
        </li>
        <li>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              document
                .querySelector(".form")
                .scrollIntoView({ behavior: "smooth" });
            }}
          >
            Contact
          </a>
        </li>
      </ul>
      <div className="social">
        <a href="https://facebook.com" target="_blank">
          <img src={Facebook} />
        </a>
        <a href="https://instagram.com" target="_blank">
          <img src={Instagram} />
        </a>
        <a href="https://twitter.com" target="_blank">
          <img src={Twitter} />
        </a>
      </div>
      <div className="design">
        Design with love © Tanahcon 2019. All right reserved
      </div>
    </StyledFooter>
  );
};
