import React from "react";
import styled from "styled-components";
import About from "./main/About.jsx";
import Clients from "./main/Clients.jsx";
import Header from "./main/Header.jsx";
import Hero from "./main/Hero.jsx";
import Portfolio from "./main/Portfolio.jsx";
import Contact from "./main/Contact.jsx";
import Footer from "./main/Footer.jsx";

const StyledApp = styled.div``;
export default () => {
  return (
    <StyledApp>
      <Header />
      <Hero />
      <About />
      <Portfolio />
      <Clients />
      <Contact />
      <Footer />
    </StyledApp>
  );
};
