import React from "react";
import styled from "styled-components";
import Variables from "../../variables.js";

const StyledSkill = styled.div`
  margin-left: 15px;
  margin-right: 15px;
  margin-top: 80px;
  border-radius: 18px;
  border: 1px solid transparent;
  transition: all 1s linear;
  box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.1);
  position: relative;
  top: 0px;

  &:hover {
    top: -40px;
    border: 1px solid #d8d8d8;
    box-shadow: 0px 20px 80px rgba(0, 0, 0, 0.15);
  }

  .card {
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    width: 306px;
    height: 343px;
    padding: 55px 20px;
    border-radius: 18px;
    background-color: #fff;
    ${Variables.media.mobile} {
      width: 100%;
      text-align: center;
    }

    img {
      width: 76px;
      height: 78px;
      margin-bottom: 30px;
    }

    .title {
      margin: 0;
      margin-bottom: 20px;
      font-size: 24px;
      font-weight: 500;
      line-height: 32px;
      color: ${Variables.colors.textPrimary};
    }

    .descr {
      margin: 0px;
      font-size: 18px;
      line-height: 28px;
      color: ${Variables.colors.textSecondary};
    }
  }
`;
export default (props) => {
  return (
    <StyledSkill>
      <div className="card">
        <img src={props.image} />
        <p className="title">{props.title}</p>
        <p className="descr">{props.descr}</p>
      </div>
    </StyledSkill>
  );
};
