import React, { useState } from "react";
import styled from "styled-components";
import Button from "../common/Button.jsx";
import Variables from "../../variables.js";

const StyledPortfolio = styled.div`
  color: #ffffff;

  .text-wrapper {
    padding: 136px 138px 110px 250px;
    background: ${Variables.colors.primary};
    ${Variables.media.touch} {
      padding: 136px 20px 110px 20px;
    }
    .title {
      margin: 0;
      margin-bottom: 18px;
      font-size: 60px;
      font-weight: bold;
      line-height: 80px;
      letter-spacing: -1.76471px;
      ${Variables.media.touch} {
        font-size: 30px;
      }
    }
    .subtitle {
      max-width: 330px;
      margin: 0;
      margin-bottom: 10px;
      font-size: 15px;
      line-height: 24px;
    }
    .categories {
      display: flex;
      justify-content: space-between;
      align-items: baseline;
      ${Variables.media.mobile} {
        flex-direction: column;
      }
      .category {
        .category-title {
          padding-bottom: 3px;
          border-bottom: 2px solid transparent;
          margin-bottom: 20px;
          margin-right: 40px;
          display: inline-block;
          font-size: 18px;
          line-height: 23px;
          transition: border-bottom 0.5s linear;
          user-select: none;
          cursor: pointer;
          &.active {
            border-bottom: 2px solid #ffffff;
          }
        }
      }
    }
  }

  .projects {
    display: flex;
    flex-flow: row wrap;
    width: 100%;
    padding-top: 20px;
    padding-bottom: 20px;
    overflow: hidden;
    position: relative;
    top: -20px;
    .project-wrapper {
      flex: auto;
      height: 300px;
      min-width: 426px;
      cursor: pointer;
      &:hover {
        z-index: 10;
      }
      .project {
        height: 300px;
        width: 100%;
        position: relative;
        left: 0;
        top: 0;
        overflow: hidden;
        display: flex;
        background-size: cover;
        transition: all 0.5s linear;
        box-shadow: 0px 0px 0px rgba(0, 0, 0, 0);
        &:hover {
          width: calc(100% + 40px);
          height: 340px;
          left: -20px;
          top: -20px;
          box-shadow: 0px 20px 80px rgba(0, 0, 0, 0.2);
          .details {
            transition: bottom 0.5s linear;
            bottom: 0px;
          }
        }
        .details {
          position: absolute;
          width: 100%;
          height: 100px;
          bottom: -100px;
          right: 0;
          background: ${Variables.colors.primary};
          .title {
            font-size: 24px;
            line-height: 32px;
            margin-left: 48px;
            ${Variables.media.mobile} {
              font-size: 20px;
            }
          }
          .date {
            margin-top: 25px;
            margin-bottom: 10px;
            margin-left: 48px;
            font-size: 13px;
            line-height: 16px;
          }
        }
      }
    }
  }
`;

const projectsBase = [
  {
    date: "25 December 2019",
    title: "Lorem Ipsum Website Project",
    image:
      "https://images.unsplash.com/photo-1537200086021-dd85d29e229f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  },
  {
    date: "25 December 2019",
    title: "Lorem Ipsum Website Project",
    image:
      "https://images.unsplash.com/photo-1531835551805-16d864c8d311?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  },
  {
    date: "25 December 2019",
    title: "Lorem Ipsum Website Project",
    image:
      "https://images.unsplash.com/photo-1524758631624-e2822e304c36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
  },
  {
    date: "25 December 2019",
    title: "Lorem Ipsum Website Project",
    image:
      "https://images.unsplash.com/photo-1526057565006-20beab8dd2ed?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  },
  {
    date: "25 December 2019",
    title: "Lorem Ipsum Website Project",
    image:
      "https://images.unsplash.com/photo-1503602642458-232111445657?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  },
  {
    date: "25 December 2019",
    title: "Lorem Ipsum Website Project",
    image:
      "https://images.unsplash.com/photo-1502005229762-cf1b2da7c5d6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1868&q=80",
  },
  {
    date: "25 December 2019",
    title: "Lorem Ipsum Website Project",
    image:
      "https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
  },
  {
    date: "25 December 2019",
    title: "Lorem Ipsum Website Project",
    image:
      "https://images.unsplash.com/photo-1550254478-ead40cc54513?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  },
  {
    date: "25 December 2019",
    title: "Lorem Ipsum Website Project",
    image:
      "https://images.unsplash.com/photo-1551298370-9d3d53740c72?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  },
];

const categories = [
  {
    title: "UI Design",
  },
  {
    title: "UX Design",
  },
  {
    title: "Graphic Design",
  },
  {
    title: "Branding",
  },
  {
    title: "Icon",
  },
];
const shuffle = (array) => {
  let currentIndex = array.length,
    temporaryValue,
    randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
};
const projects = categories.reduce((a, v, i) => {
  a = [
    ...a,
    ...shuffle(projectsBase).map((p) => {
      p.category = i;
      return { ...p };
    }),
  ];
  return a;
}, []);

export default () => {
  const [showAll, setShowAll] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState(0);
  const [oldSelectedCategory, setOldSelectedCategory] = useState(1);
  const handleClick = (i) => () => {
    setShowAll(false);
    setOldSelectedCategory(selectedCategory);
    setTimeout(() => {
      setSelectedCategory(i);
    }, 500);
  };
  return (
    <StyledPortfolio className="portfolioSection">
      <div className="text-wrapper">
        <div className="title">Selected Portfolio</div>
        <div className="subtitle">
          Freelance UI/UX Designer, also passionate in making beautiful
          illustrations and icons
        </div>

        {/* filter */}
        <div className="categories">
          <div className="category">
            {categories.map((c, i) => (
              <div
                key={i}
                onClick={handleClick(i)}
                className={`category-title ${
                  i == selectedCategory && i != oldSelectedCategory && !showAll
                    ? "active"
                    : ""
                }`}
              >
                {c.title}
              </div>
            ))}
          </div>
          <div className="viewAll">
            <Button
              color="#fff"
              textColor={Variables.colors.primary}
              onClick={() => {
                setShowAll(true);
              }}
              text="View All"
            />
          </div>
        </div>
      </div>
      <div className="projects">
        {projects
          .filter((p) => showAll || p.category == selectedCategory)
          .map((p, i) => (
            <div key={i} className="project-wrapper">
              <div
                className="project"
                style={{ backgroundImage: `url(${p.image})` }}
              >
                <div className="details">
                  <div className="date">{p.date}</div>
                  <div className="title">{p.title}</div>
                </div>
              </div>
            </div>
          ))}
      </div>
    </StyledPortfolio>
  );
};
