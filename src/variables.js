export default {
  animation: `@keyframes float {
    0% {
      transform: translate(0px, 0px) rotate(0deg);
    }
    10% {
      transform: translate(40px, 15px) rotate(360deg);
    }
    20% {
      transform: translate(84px, 60px) rotate(0deg);
    }
    30% {
      transform: translate(15px, 45px) rotate(360deg);
    }
    40% {
      transform: translate(73px, 47px) rotate(0deg);
    }
    50% {
      transform: translate(25px, 50px) rotate(360deg);
    }
    60% {
      transform: translate(27px, 32px) rotate(0deg);
    }
    70% {
      transform: translate(34px, 10px) rotate(360deg);
    }
    80% {
      transform: translate(48px, 36px) rotate(0deg);
    }
    90% {
      transform: translate(10px, 70px) rotate(360deg);
    }
    100% {
      transform: translate(0px, 0px) rotate(0deg);
    }
  }`,
  media: {
    touch: "@media (max-width: 1023px)",
    mobile: "@media (max-width: 760px)",
    tablet: "@media (min-width: 761px) and (max-width: 1023px)",
    desktop: "@media (min-width: 1024px) and (max-width: 1279px)",
    tv: "@media (min-width: 1280px)",
  },
  colors: {
    textPrimary: "#222A41",
    textSecondary: "#6D7783",
    textTernary: "#293340",
    primary: "#3B43F2",
    bgImageColor: "#3138D1",
    bgshapes: "#B4B4B4",
  },
};
